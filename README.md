Download images from *.memebase.com. The script should find the last page of the site if you don't supply one when you run it. The biggest caveat is
making sure that there isn't a trailing / at the end of your URL in your .txt file or the command line!

Usage: python imageDownloader.py http://memebase.com (optional number of pages)

ex. python imageDownloader.py sites.txt 25 (if you want to download the first 25 pages of the list of sites you have in sites.txt)
python  imageDownloader.py http://verydemotivational.memebase.com (to download all the images from Very Demoticational)

EXAMPLE sites.txt:
http://memebase.com
http://verydemotivational.memebase.com
http://pictureisunrelated.memebase.com