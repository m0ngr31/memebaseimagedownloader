# Modified imageDownloader.py by FB36
# From http://code.activestate.com/recipes/577385-image-downloader/
# Modified by m0ngr31 (Joe Ipson) to download images semi-automatically from *.memebase.com
import urllib2
import re
import sys
from os.path import basename
from urlparse import urlsplit

global imgList
imgList = []

def getContent(url_orig, page_num):
    url = url_orig + "/page/" + str(page_num) + "/"
    
    try:
        urlContent = urllib2.urlopen(url).read()
        return urlContent
    except:
        return 0

def downloadImages(urlCont):
    imgUrls = re.findall("<img class='event-item-lol-image' src='(.*?)'", urlCont)

    for imgUrl in imgUrls:
        try:
            if basename(urlsplit(imgUrl)[2]) in imgList:
                print("Skipping: " + basename(urlsplit(imgUrl)[2]))
            elif basename(urlsplit(imgUrl)[2]).find('.') == -1:
                print("Skipping: " + basename(urlsplit(imgUrl)[2]))
                imgList.append(basename(urlsplit(imgUrl)[2]))
            else:
                imgData = urllib2.urlopen(imgUrl).read()
                fileName = basename(urlsplit(imgUrl)[2])
                print('Downloading: ' + fileName)
                output = open(fileName,'wb')
                output.write(imgData)
                output.close()
                imgList.append(fileName)
        except:
            pass    

# main

if len(sys.argv) > 2:
    pages = sys.argv[2]
else:
    #cont1 = getContent(sys.argv[1], 1)
    if '.txt' in sys.argv[1]:
        a = open(sys.argv[1], 'r')
        #firstURL = a.readline()
        a.close()
        
        #print firstURL
        pages1 = re.findall('href="(.*?)">Last', getContent(firstURL.rstrip(), 1))
    else:
        pages1 = re.findall('href="(.*?)">Last', getContent(sys.argv[1], 1))

    pageLen = len(pages1[0])
    #print pageLen
    pages = (pages1[0])[pageLen-5:]
    #print pages
    pages = pages[:-1]
    #print int(pages)

if '.txt' in sys.argv[1]:
    f = open(sys.argv[1], 'r')
    for line in f:
        print ("--------------------------------")
        print line.rstrip()
        print ("--------------------------------")
        cont = getContent(line.rstrip(), 1)
        for x in xrange(1,int(pages)+1):
            #downloadImages(line.rstrip(), x)
            if x < 10:
                print ("=============")
                print ("| On Page " + str(x) + " |")
                print ("=============")
            elif x > 99:
                print ("===============")
                print ("| On Page " + str(x) + " |")
                print ("===============")
            elif x > 999:
                print ("================")
                print ("| On Page " + str(x) + " |")
                print ("================")
            else:
                print ("==============")
                print ("| On Page " + str(x) + " |")
                print ("==============")
            #cont = getContent(line.rstrip(), x)
            downloadImages(getContent(line.rstrip(), x))
else:
    for x in xrange(1,int(pages) + 1):
        if x < 10:
            print ("=============")
            print ("| On Page " + str(x) + " |")
            print ("=============")
        elif x > 99:
            print ("===============")
            print ("| On Page " + str(x) + " |")
            print ("===============")
        elif x > 999:
            print ("================")
            print ("| On Page " + str(x) + " |")
            print ("================")
        else:
            print ("==============")
            print ("| On Page " + str(x) + " |")
            print ("==============")
        downloadImages(getContent(sys.argv[1], x))